terraform {
  backend "s3" {
    bucket = "task-bucket-2"
    region = "ap-south-1"
    key = "terraform.tfstate123"
  }
}

provider "aws" {
  region = var.region
}
module "my_vpc_module" {
    source = "./Modules/vpc"
    project = var.project
    vpc_cidr = var.vpc_cidr
    env = var.enviroment
    pri_sub_cidr = var.private_cidr
    pub_sub_cidr = var.public_cidr
}
resource "aws_security_group" "my_sg" {
  name = "${var.project}-sg"
  vpc_id = module.my_vpc_module.vpc_id
  description = "allow http service"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
   depends_on = [ 
    module.my_vpc_module
    ]
}

  module "my_instance_module" {
   source = "./Modules/instance"
   instance_count = 2
   image_id = var.image_id
   instance_type = var.instance_type
   key_pair = var.key_pair
   project = var.project
   env = var.enviroment
   subnet_id = module.my_vpc_module.pub_subnet_id
   my_sg_ids = ["aws_security_group.my_sg.id"]
}

module "my_instance-private" {
  source = "./Modules/instance"
  instance_count = 2
  image_id = var.image_id
  instance_type = var.instance_type
  key_pair = var.key_pair
  project = var.project
  env = var.enviroment
  subnet_id = module.my_vpc_module.pri_subnet_id
  my_sg_ids       = ["aws_security_group.my_sg.id"]
}