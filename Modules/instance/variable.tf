variable "instance_count" {}
variable "image_id" {}
variable "instance_type" {}
variable "my_sg_ids" {
type = list
}
variable "key_pair" {}
variable "project" {}
variable "env" {}
variable "subnet_id" {}